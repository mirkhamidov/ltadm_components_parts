<?
$items=array(
  "id_lt_languages" => array(
         "desc" => "Язык",
         "type" => "select_from_table",
         "table" => "lt_languages",
         "key_field" => "id_lt_languages",
         "fields" => array("name"),
         "show_field" => "%1",
         "condition" => "",
         "order" => array ("orders" => "ASC","name" => "ASC"),
       ),
  "name" => array(
         "desc" => "Название фрагмента",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
         ),
       ),
  "url" => array(
         "desc" => "Идентификатор фрагмента",
         "type" => "text",
         "maxlength" => "50",
         "size" => "30",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[A-Za-z0-9_\-:/~]+$",
             "flags" => "g",
             "error" => "Только латинские символы, цифры и спецсимволы!",
           ),
         ),
       ),

  "txt" => array(
         "desc" => "Содержание",
         "type" => "editor",
         "blank_window" => true,
		"show" => array ("type" => "textarea","width" => "100%","height" => "10", "style" => "width:100%"),
         "width" => "100%",
         "height" => "10",
         "select_on_edit" => true,
       ),
);
?>