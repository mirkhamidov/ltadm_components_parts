<?
$config=array(
	"name"		=> "Отдельные блоки контента на сайте",
	"menu_icon"	=> "icon-puzzle",
	"status"	=> "system",
	"windows"	=> array(
		"create"	=> array("width" => 1000,"height" => 700),
		"edit"		=> array("width" => 1000,"height" => 500),
		"list"		=> array("width" => 600,"height" => 500),
	),
	"right"				=> array("admin","#GRANTED"),
	"main_table"		=> "lt_parts",
	"list"				=> array(
		"name"				=> array("isLink" => true),
		"url"				=> array(),
		"id_lt_languages"	=> array("align" => "center"),
	),
	"select" => array(
		"max_perpage" => 20,
		"default_orders" => array(
			array("id_lt_languages" => "ASC"),
			array("name" => "ASC")
		),
		"default" => array(
			"id_lt_parts" => array(
				"desc"		=> "Фрагмент",
				"type"		=> "select_from_tables_as_tree",
				"use_empty"	=> true,
				"in_list"	=> true,
				"links"		=> array(
					array(
						"table"			=> "lt_languages",
						"key_field"		=> "id_lt_languages",
						"fields"		=> array("name"),
						"show_field"	=> "%1",
						"order"			=> array ("name" => "ASC"),
					),
					array(
						"table"			=> "lt_parts",
						"key_field"		=> "id_lt_parts",
						"parent"		=> "id_lt_languages",
						"fields"		=> array("name", "url"),
						"show_field"	=> "%1 (%2)",
						"order"			=> array ("name" => "ASC"),
					),
				),
			),
		),
	),
);

$actions=array(
	"create" => array(
		"before_code" => "",
	),
	"edit" => array(
		"before_code" => "",
	),
);

?>